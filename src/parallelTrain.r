#
#   parallelTrain.r of project AmazonReviews
#     a slight reshape of the original train_models function from RTextTools
#     but with parallel support if available
#   
#   version 20160825
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

parallelTrain = function (container, algorithms, tuned = FALSE, ...) {
  
  result = list()
  
  require(parallel)
  nbCores <- detectCores(all.tests = FALSE, logical = TRUE)
  if (!is.na(nbCores)) {
    require(doParallel)
    cl <- makeCluster(nbCores)
    registerDoParallel(cl)
    if (tuned) {
      paraRes <- foreach (algorithm = algorithms) %dopar% {
        source("src/tuned_train_model.r")
        model = tuned_train_model(container, algorithm)
      }
    }
    else {
      paraRes <- foreach (algorithm = algorithms) %dopar% {
        require(RTextTools)
        model = train_model(container, algorithm, ...)
      }
    }
    result[algorithms] = paraRes[1:length(algorithms)]
    stopCluster(cl)
  }
  else {
    if (tuned) {
      source("src/tuned_train_model.r")
      for (algorithm in algorithms) {
        model = tuned_train_model(container, algorithm)
        result[[algorithm]] = model
      }
    }
    else
      require(RTextTools)
      result <- train_models(container, algorithms, ...)
  }
  
  return(result)
}
