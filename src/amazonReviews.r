#
#   amazonReviews.r of project AmazonReviews
#     trains the prepared data, classifies the test sample and computes a perfomance
#     comparison between various machine learning algorithms
#   
#   version 20160904
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

source("src/getAndCleanData.r")
source("src/prepareData.r")
source("src/tunedTrainModels.r")

algorithmsAll = c("BAGGING", "BOOSTING", "GLMNET", "MAXENT", "NNET", "RF", "SLDA", "SVM", "TREE")
algorithmsLowMemory = c("GLMNET", "MAXENT", "RF", "SLDA", "SVM", "TREE")
algorithmsFast = c("GLMNET", "SLDA", "SVM", "TREE")

# choose the data set
dataList <- list("data/always", "data/gillette", "data/oral-b", "data/pantene", "data/tampax", "data" )
dataToTrain <- 6

# substitute positive and negative words ?
for (substitutePosNeg in c(TRUE, FALSE)) {
  
  # ~1/4, ~1/2, full amount of attributes for the big data set, no influence on smaller ones
  attributes <- list(0.999, 0.9998, 0)
  for (desiredAttributes in 1:3) {
    
    consistentData <- getAndCleanData(dataList[[dataToTrain]])
    
    trainAndTestData <- prepareData(consistentData, 0.1, substitutePosNeg, attributes[[desiredAttributes]])
    
    if (desiredAttributes <= 2 | dataToTrain != 6) {
      modelStock <- train_models(trainAndTestData, algorithmsAll)
    } else {
      modelStock <- train_models(trainAndTestData, algorithmsLowMemory)
    }
    predictionStock <- classify_models(trainAndTestData, modelStock)
    resultStock <- create_analytics(trainAndTestData, predictionStock)
    summary(resultStock)
    filename <- paste("measures/", dataToTrain, ifelse(substitutePosNeg, "-posneg", "-naive"), "-stock-", desiredAttributes, ".dat", sep = "")
    save(resultStock, file = filename)
    
    if (desiredAttributes <= 2 | dataToTrain != 6) {
      modelTuned <- tuned_train_models(trainAndTestData, algorithmsAll)
    } else {
      modelTuned <- tuned_train_models(trainAndTestData, algorithmsLowMemory)
    }
    predictionTuned <- classify_models(trainAndTestData, modelTuned)
    resultTuned <- create_analytics(trainAndTestData, predictionTuned)
    summary(resultTuned)
    filename <- paste("measures/", dataToTrain, ifelse(substitutePosNeg, "-posneg", "-naive"), "-tuned-", desiredAttributes, ".dat", sep = "")
    save(resultTuned, file = filename)
  }
}