#
#   getAndCleanData.r of project AmazonReviews
#     gets the raw data from data files and prepares it for further analysis through 
#     multiple machine learning algorithms.
#   
#   version 20160825
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

source("src/multMerge.r")

getAndCleanData = function(pathToData) {
  # get data from data files that have valid user rating
  data <- subset(multMerge(pathToData),
                 user_rating %in% c("1", "2", "3", "4", "5"))
  
  # sort data by user rating
  data <- data[order(data[,1]),]
  
  # format user ratings as number
  data$user_rating <- as.double(data$user_rating)
  
  # format user reviews to lower case, no punctuation, no digits and single space between words
  Encoding(data$review) <- "latin1"
  data$review <- iconv(data$review, from = "latin1", to = "ASCII", sub = " ")
  data$review <- tolower(data$review)
  data$review <- gsub("[,.;]", " ", data$review)
  data$review <- gsub("[[:punct:]]", "", data$review)
  data$review <- gsub("[[:digit:]]", " ", data$review)
  data$review <- gsub("^ *|(?<= ) | *$", "", data$review, perl = TRUE)
  
  return(data)
}
