tuned_train_models <- function (container, algorithms, ...) 
{
  source("src/tunedTrainModel.r")
  result = list()
  for (algorithm in algorithms) {
    model = tuned_train_model(container, algorithm, ...)
    result[[algorithm]] = model
  }
  return(result)
}