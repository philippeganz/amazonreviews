#
#   multMerge.r of project AmazonReviews
#     reads all files in the provided path and combines them into a single dataframe
#     containing the user_rating and review columns
#   
#   version 20160808
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

multMerge = function(pathToData){
  filenames <- list.files(path=pathToData, full.names=TRUE, recursive = TRUE)
  datalist <- lapply(filenames, 
                     function(x){
                       read.csv(blank.lines.skip = TRUE,
                                colClasses = c("NULL", "NULL", "NULL",
                                               "character", "NULL", "NULL", 
                                               "NULL", "character"),
                                file=x,
                                fileEncoding="latin1",
                                header=TRUE,
                                sep="\t",
                                skipNul = TRUE)
                     }
  )
  Reduce(function(x,y) {rbind(x,y)}, datalist)
}