#
#   tuned_train_model.r of project AmazonReviews
#     a slight reshape of the original train_model function from RTextTools
#     but with the use of tune where available
#   
#   version 20160903
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

tuned_train_model <- function (container, algorithm)
{
  print(paste("Computing ", algorithm, "...", sep = ""))
  ptm <- proc.time()
  gc()
  if (algorithm == "BAGGING") {
    require(ipred)
    model <- bagging(container@training_codes ~ ., 
                     data = data.frame(as.matrix(container@training_matrix)),
                     nbagg = 5)
    shortTime <- (proc.time() - ptm)[3]
    nbagg <- min(max(as.integer(5 * 900/shortTime), 10),300)
    model <- bagging(container@training_codes ~ ., 
                     data = data.frame(as.matrix(container@training_matrix)),
                     nbagg = nbagg)
  }
  else if (algorithm == "BOOSTING") {
    require(caTools)
    model <- LogitBoost(xlearn = as.matrix(container@training_matrix), 
                        ylearn = container@training_codes,
                        nIter = max(as.integer(dim(container@training_matrix)[2]/10),100))
  }
  else if (algorithm == "GLMNET") {
    require(glmnet)
    training_matrix <- as(container@training_matrix, "sparseMatrix")
    model <- glmnet(x = training_matrix, 
                    y = container@training_codes, 
                    family = "multinomial",
                    thresh = 1e-08,
                    maxit = 1e7)
  }
  else if (algorithm == "MAXENT") {
    require(maxent)
    params <- tune.maxent(container@training_matrix, 
                          as.vector(container@training_codes),
                          nfold = 10)
    model <- maxent(container@training_matrix, 
                    container@training_codes, 
                    l1_regularizer = params[1], 
                    l2_regularizer = params[2], 
                    use_sgd = params[3], 
                    set_heldout = params[4])
  }
  else if (algorithm == "NNET") {
    require(nnet)
    attrib.amnt <- dim(container@training_matrix)[2]
    size <- min(max(as.integer(35000/attrib.amnt), 3),10)
    model <- nnet( container@training_codes ~ ., 
                   data = data.frame(as.matrix(container@training_matrix)),
                   size = size,
                   entropy = TRUE,
                   rang = 0.1,
                   decay = 0,
                   maxit = 100,
                   trace = TRUE,
                   MaxNWts = attrib.amnt*(size + 1))
  }
  else if (algorithm == "RF") {
    require(randomForest)
    model <- randomForest(x = as.matrix(container@training_matrix), 
                          y = container@training_codes,
                          ntree = 5)
    shortTime <- (proc.time() - ptm)[3]
    treeAmnt <- min(max(as.integer(5 * 900/shortTime), 10),300)
    model <- randomForest(x = as.matrix(container@training_matrix), 
                          y = container@training_codes,
                          ntree = treeAmnt )
  }
  else if (algorithm == "SLDA") {
    require(ipred)
    model <- slda(container@training_codes ~ ., 
                  data = data.frame(as.matrix(container@training_matrix)))
  }
  else if (algorithm == "SVM") {
    require(e1071)
    model <- best.svm(x = container@training_matrix,
                      y = container@training_codes,
                      probability = TRUE)
  }
  else if (algorithm == "TREE") {
    require(tree)
    model <- tree(container@training_codes ~ ., 
                  data = data.frame(as.matrix(container@training_matrix)))
  }
  else {
    stop("ERROR: Invalid algorithm specified. Type print_algorithms() for a list of available algorithms.")
  }
  print(proc.time() - ptm)
  gc()
  return(model)
}