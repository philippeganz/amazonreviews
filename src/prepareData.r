#
#   prepareData.r of project AmazonReviews
#     prepares the raw data for further analysis through multiple machine 
#     learning algorithms
#   
#   version 20160903
#   
#   Copyright (c) 2016, Philippe Ganz
#   University of Geneva
#
#   under 3-clause BSD license
#

require(RTextTools)

prepareData = function(consistentData, testRatio = 0.1, withPosNegWords = FALSE, sparsityRemoval = 0.9999) {
  
  preparedReviews <- as.character(consistentData[,2])
  
  if (withPosNegWords) {
    positif <- scan("pattern/PosWords", what = "character", sep = "\n")
    for( i in 1:length(positif)) {
      preparedReviews <- gsub(paste("\\<",positif[i],"\\>",sep = ""),
                              "good", preparedReviews)
    }
    
    negatif <- scan("pattern/NegWords", what = "character", sep = "\n")
    for( i in 1:length(negatif)) {
      preparedReviews <- gsub(paste("\\<",negatif[i],"\\>",sep = ""),
                              "bad", preparedReviews)
    }
  }
  
  preparedReviews <- create_matrix(textColumns = preparedReviews, 
                                   language = "english", 
                                   minDocFreq = 1,
                                   maxDocFreq = Inf, 
                                   minWordLength = 3, 
                                   maxWordLength = Inf, 
                                   ngramLength = 1, 
                                   removeNumbers = FALSE, 
                                   removePunctuation = FALSE, 
                                   removeSparseTerms = sparsityRemoval,
                                   removeStopwords = TRUE, 
                                   stemWords = TRUE,
                                   stripWhitespace = FALSE, 
                                   toLower = FALSE, 
                                   weighting = tm::weightTfIdf
  )
  
  # take a subset of each user rating
  randomLines = vector(mode = "logical", length = dim(consistentData[1])[1])
  for (star in 1:5) {
    randomLines[which(consistentData[,1] == star)] = runif(which(consistentData[,1] == star)) > testRatio
  }
  
  trainAndTestData <- create_container(matrix = preparedReviews, 
                                       labels = consistentData[,1], 
                                       trainSize = randomLines, 
                                       testSize = !randomLines,
                                       virgin = FALSE)
}
